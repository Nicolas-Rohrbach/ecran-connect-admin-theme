<?php ?>

<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php bloginfo('name'); ?></title>
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
</head>
<!-- BODY -->
<body <?php body_class(); ?>>
<!-- HEADER -->
<header>
	<!-- NAV -->
    <nav class="navbar navbar-expand-lg navbar-dark nav_ecran">
        <a class="navbar-brand" href="<?php echo get_home_url(); ?>">
	        <?php if (get_header_image()) : ?>
                <img src="<?php header_image(); ?>" class="d-inline-block align-top logo" alt="Logo du site">
	        <?php endif; ?>
	        <?php bloginfo('name'); ?>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- NAV CONTENT -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item dropdown active">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Informations</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?php echo esc_url(get_permalink(get_page_by_title('Créer une information'))); ?>">Créer une information</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?php echo esc_url(get_permalink(get_page_by_title('Gestion des informations'))); ?>">Voir mes informations</a>
                    </div>
                </li>
                <li class="nav-item dropdown active">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Alertes</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?php echo esc_url(get_permalink(get_page_by_title('Créer une alerte'))); ?>">Créer une alerte</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?php echo esc_url(get_permalink(get_page_by_title('Gestion des alertes'))); ?>">Voir mes alertes</a>
                    </div>
                </li>
                <li class="nav-item dropdown active">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Utilisateurs</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?php echo esc_url(get_permalink(get_page_by_title('Créer un utilisateur'))); ?>">Créer un utilisateur</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?php echo esc_url(get_permalink(get_page_by_title('Gestion des utilisateurs'))); ?>">Voir mes utilisateurs</a>
                    </div>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php if(is_user_logged_in()) : ?>
                    <li class="nav-item active">
                        <a class="nav-link" href="<?php echo wp_login_url(); ?>"><?php echo wp_get_current_user()->user_login; ?></a>
                    </li>
                    <li class="nav-item active my-2 my-lg-0">
                        <a class="nav-link" href="<?php echo wp_logout_url(); ?>">Déconnexion</a>
                    </li>
                <?php else : ?>
                    <li class="nav-item active">
                        <a class="nav-link" href="<?php echo wp_login_url(); ?>">Connexion</a>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
    </nav>
</header>