<?php

function add_scripts()
{
	//jQuery
	wp_enqueue_script('jquery_cdn', 'https://code.jquery.com/jquery-3.4.1.slim.min.js');

	//Bootstrap
	wp_enqueue_style('bootstrap_css', 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css');
	wp_enqueue_script('bootstrap_js', 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js', array('jquery_cdn'), '', true);

	// CSS
	wp_enqueue_style( 'style_ecran', get_template_directory_uri().'/style.css');
	wp_enqueue_style( 'header_ecran_theme', get_template_directory_uri().'/assets/css/header.css');
	wp_enqueue_style( 'content_ecran_theme', get_template_directory_uri().'/assets/css/content.css');
	wp_enqueue_style( 'sidebar_ecran_theme', get_template_directory_uri().'/assets/css/sidebar.css');
	wp_enqueue_style( 'footer_ecran_theme', get_template_directory_uri().'/assets/css/footer.css');
}
add_action('wp_enqueue_scripts', 'add_scripts');


/**
 * CSS for login page
 */
function my_login_stylesheet()
{
	wp_enqueue_style('login_css', get_stylesheet_directory_uri() . '/assets/css/login.css');
}
add_action('login_enqueue_scripts', 'my_login_stylesheet');

/**
 * Remove the wordpress bar
 */
add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar()
{
	if (!current_user_can('administrator') && !is_admin()) {
		show_admin_bar(false);
	}
}

/**
 * Disable the url /wp-admin except for the admin
 */
function wpm_admin_redirection()
{
	if (is_admin() && !current_user_can('administrator')) {
		wp_redirect(home_url());
		exit;
	}
}
add_action('init', 'wpm_admin_redirection');

/**
 * Change the url of the logo
 * @return mixed
 */
function my_login_logo_url()
{
	return $_SERVER['HTTP_HOST'];
}
add_filter('login_headerurl', 'my_login_logo_url');

/**
 * Change the title of the logo
 *
 * @return string
 */
function my_login_logo_url_title()
{
	return get_bloginfo('name');
}
add_filter('login_headertitle', 'my_login_logo_url_title');


// Register a new sidebar simply named 'sidebar'
function add_widget_Support()
{
	register_sidebar( array(
		'name'          => 'Sidebar',
		'id'            => 'sidebar',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	) );
}
add_action('widgets_init', 'add_Widget_Support');

// Register a new navigation menu
function add_Main_Nav()
{
	register_nav_menu('header-menu',__('Header Menu'));
}
add_action('init', 'add_Main_Nav');

$args = array(
	'flex-width'    => true,
	'width'         => 500,
	'flex-height'   => true,
	'height'        => 500,
	'default-image' => get_template_directory_uri() . '/assets/media/header.png',
);
add_theme_support('custom-header', $args);

/*
$args = array(
	'default-color' => '0000ff',
	'default-image' => get_template_directory_uri() . '/assets/media/background.png',
);
add_theme_support('custom-background', $args);
*/