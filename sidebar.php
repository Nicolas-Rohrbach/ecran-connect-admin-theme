<?php if ( is_active_sidebar( 'sidebar' ) ) : ?>
    <!-- SIDEBAR -->
	<aside id="sidebar_right" class="sidebar_ecran" role="complementary">
		<?php dynamic_sidebar( 'sidebar' ); ?>
	</aside>
<?php endif; ?>